import React, { Component } from "react";

class TableComponent extends Component {
    render() {
      return (
        <div className="tableClass">        
          <h1> Table </h1>
            <table className="crud-table">
            <thead>
                <tr>
                <th>Name</th>
                <th>Color</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
            {this.props.tableBody.map((item, index) => (
              <tr key={item.id + index}>
                <td> {item.name} </td>
                <td> {item.color} </td>                
                <td>
                  <button
                    className="btn save-btn"
                    onClick={() => this.props.update(item)}
                  >
                    Edit
                  </button>                  
                  <button
                    className="btn del-btn"  onClick={() => { if (window.confirm('Are you sure you wish to delete: '+item.name+'?')) this.props.delete(item) } }
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
            </tbody>
            </table>
        </div>
      );
    }
  }

export default TableComponent;