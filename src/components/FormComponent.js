import React, { Component } from "react";

class FormComponent extends Component {
    render() {
      return (
        <div className="formClass">        
          <form onSubmit={this.props.handleSubmit}>
          <input
                  type="hidden"
                  value={this.props.updateId}
                  placeholder="Enter Name"
                  
              />
              <label>Name</label>
              <input
                  type="text"
                  value={this.props.name}
                  placeholder="Enter Name"
                  onChange={this.props.handleChangeName}
              />
              <br />
              <label>Color</label>
              <select className="form-select" value={this.props.color} onChange={this.props.handleChangeColor}>
                      <option value="Red">Red</option>
                      <option value="Green">Green</option>
                      <option value="Blue">Blue</option>
                  </select>
              <br />
            <button type="submit" className="btn save-btn">
              Save
            </button>
            <button type="button" className="btn clear-btn" onClick={this.props.handleResetForm}>
              Clear
            </button>
          </form>
        </div>
      );
    }
  }

export default FormComponent;