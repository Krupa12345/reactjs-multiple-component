import React, { Component } from "react";

class CircleComponent extends Component {
    render() {
      return (
        <div className="dotClass">
            <span className="dotRed"><div className="lableclass">{this.props.countRed}</div></span>
            <span className="dotBlue"><div className="lableclass">{this.props.countBlue}</div></span>
            <span className="dotGreen"><div className="lableclass">{this.props.countGreen}</div></span>
        </div>
      );
    }
  }

export default CircleComponent;