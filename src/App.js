//import logo from './logo.svg';
import './App.css';
import "./index.css";
import React, { Component } from "react";
import FormComponent from './components/FormComponent';
import CircleComponent from './components/CircleComponent';
import TableComponent from './components/TableComponent';

class MainData extends React.Component {
  constructor() {
    super();
    this.state = {
      lists: [
          {id: 1, name: 'A', color: 'Red'},
          {id: 2, name: 'B', color: 'Green'}
      ],
      countRed: '1',
      countGreen: '1',
      countBlue: '0',
      name: "",
      color: "Red",
      updateId: "0",
      lastid:3,
    };
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleResetForm = this.handleResetForm.bind(this);
    this.countBall = this.countBall.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }

  render() {    
    return (
      <div className="App">
       <FormComponent handleChangeName={this.handleChangeName} handleChangeColor={this.handleChangeColor} handleSubmit={this.handleSubmit} handleResetForm={this.handleResetForm}  name={this.state.name} color={this.state.color} updateId={this.state.updateId}/>
       <CircleComponent countRed={this.state.countRed} countGreen={this.state.countGreen} countBlue={this.state.countBlue}/>
       <TableComponent tableBody={this.state.lists} update={this.update} delete={this.delete}/>
     </div>
    );
  }

  countBall(){
    var redCount = 0;
    var blueCount = 0;
    var greenCount = 0;
    this.state.lists.map(function(elementOfArray, indexInArray) {
        if (elementOfArray.color === 'Red') {            
            redCount ++;
        }
        if (elementOfArray.color === 'Blue') {            
            blueCount ++;
        }
        if (elementOfArray.color === 'Green') {            
            greenCount ++;
        }
    });    
    this.setState({ countRed: redCount });
    this.setState({ countBlue: blueCount });
    this.setState({ countGreen: greenCount });
  }

  delete(e) {
    this.state.lists.splice(this.state.lists.indexOf(e), 1);
    this.setState({ lists: this.state.lists });
    this.countBall();
  }
  
  update(e) {
    this.setState({ updateId: e.id });
    this.setState(e);
  }
 
  handleChangeName(e) {
    this.setState({ name: e.target.value });
    }
    handleChangeColor(e) {    
        this.setState({ color: e.target.value });
    }
handleResetForm(e){
    this.setState({ name: '' });
    this.setState({ color: 'Red' });
    this.setState({ updateId: '' });
    this.countBall();
}
  handleSubmit(e) {
    e.preventDefault();
        if (this.state.name === '') {
            alert('Both Field Are Required !!!');
          return false;
        }
        if(this.state.color === ''){
            alert('Both Field Are Required !!!');
            return false;
        }
        if (this.state.updateId === '0' || this.state.updateId === ''){
            var added=false;
            var productID = this.state.name;
            this.state.lists.map(function(elementOfArray, indexInArray) {
                if (elementOfArray.name === productID) {            
                    added = true;
                }
            });            
            if(added){
                alert(productID + ' Name is already Exists..Please enter Unique name !!!');
                return false;
            }
            const newItem = {
                name: this.state.name,
                color: this.state.color,
                id: this.state.lastid
            };
            
            this.state.lists.unshift(newItem);
            this.setState({ lists: this.state.lists });
            
        }else{
            var added1=false;
            var productID1 = this.state.name;
            var updateId = this.state.updateId;
            this.state.lists.map(function(elementOfArray, indexInArray) {
                if (elementOfArray.name === productID1 && elementOfArray.id !== updateId) {            
                    added1 = true;
                }
            });            
            if(added1){
                alert(productID1 + ' Name is already Exists..Please enter Unique name !!!');
                return false;
            }

            const updateItem = {
                name: this.state.name,
                color: this.state.color,
                id: this.state.updateId,
              };
              const index = this.state.lists.findIndex(p => p.id === this.state.updateId)
              this.state.lists[index] = updateItem;
                if(index === -1) {

                } else {
                    this.state.lists[index] = updateItem;
                }
              this.setState({ lists: this.state.lists });

        }
        var lastid = parseInt(this.state.lastid) + parseInt(1);
        this.setState({ lastid: lastid });
        this.handleResetForm();
        this.countBall();
  }
}

export default MainData;
